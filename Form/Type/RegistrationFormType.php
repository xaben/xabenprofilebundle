<?php

namespace Xaben\ProfileBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;

class RegistrationFormType extends BaseType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // add your custom field
        $builder->add('prenume', null, array('label' => 'form.prenume', 'translation_domain' => 'FOSUserBundle'));
        $builder->add('nume', null, array('label' => 'form.nume', 'translation_domain' => 'FOSUserBundle'));
        $builder->add('gender', 'choice', array(
                                'choices'   => array('m' => 'form.gender.male', 'f' => 'form.gender.female'),
                                'required'  => true,
                                'label' => 'form.gender.label',
                                'translation_domain' => 'FOSUserBundle'));
        parent::buildForm($builder, $options);
    }

    public function getName()
    {
        return 'xaben_user_registration';
    }
}
