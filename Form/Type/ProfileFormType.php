<?php

namespace Xaben\ProfileBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\ProfileFormType as BaseType;

class ProfileFormType extends BaseType
{
    protected function buildUserForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('prenume', null, array('label' => 'form.prenume', 'translation_domain' => 'FOSUserBundle'))
                ->add('nume', null, array('label' => 'form.nume', 'translation_domain' => 'FOSUserBundle'))
                ->add('gender', 'choice', array(
                'choices'   => array('m' => 'form.gender.male', 'f' => 'form.gender.female'),
                'required'  => true,
                'label' => 'form.gender.label',
                'translation_domain' => 'FOSUserBundle'));
        parent::buildUserForm($builder, $options);
    }

    public function getName()
    {
        return 'xaben_user_profile';
    }
}