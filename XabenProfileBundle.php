<?php

namespace Xaben\ProfileBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class XabenProfileBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
