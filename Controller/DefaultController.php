<?php

namespace Xaben\ProfileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class DefaultController extends Controller
{
    //returns csrf token for login form
    public function tokenAction()
    {
        //check if token is accesed directy or not
        $request = $this->container->get('request');
        $routeName = $request->get('_route');

        if ($routeName !== 'inline_token') {
            return new Response($this->container->get('form.csrf_provider')
                                 ->generateCsrfToken('authenticate'));
        } else {
            throw new AccessDeniedHttpException();
        }
    }
}
