XabenForumBundle
================

Sandbox ProfileBundle that extends on FOS User Bundle

[![Build Status](https://travis-ci.org/xaben/XabenProfileBundle.png)](https://travis-ci.org/xaben/XabenProfileBundle)

Installation
------------

Download XabenProfileBundle and its dependencies to the ``vendor`` directory. You
can use Composer for the automated process:

```console
php composer.phar require xaben/profile-bundle --no-update
php composer.phar update
```

Next, be sure to enable the bundle in your AppKernel.php file:

```php
// app/AppKernel.php
public function registerBundles()
{
    return array(
        // ...
        new Xaben\ProfileBundle\XabenProfileBundle(),
        // ...
    );
}
```

Now, install the assets from the bundle:

```console
php app/console assets:install web
```

Import the forum routes into your routing file:

```yaml
# app/config/routing.yml
xaben_forum:
    resource: "@XabenProfileBundle/Resources/config/routing.yml"
```